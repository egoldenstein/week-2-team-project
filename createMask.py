"""
createMask.py

Authors: Jakob Orel, Elizabeth Gardner, Ella Nelson, Emma Goldenstein
Date: 10-21-20

This file contains functions to create a gradient using numpy and a function
to return a black and white horizontal gradient image.

"""

import numpy as np
from PIL import Image


def get_gradation_2d(start, stop, width, height, is_horizontal):
    """Creates 2d gradient using np.tile and np.linspace.

    This function uses numpy.tile and numpy.linspace to distribute the
    values gradually. Linspace equally distributes values of an array.
    Tile arranges array vertically and horizontally.
    .T transposes the array to make it vertical.
    Parameters
    ----------
    start : int
        The starting value for gradation
    stop : int
        The stopping value for gradation
    width : int
        The width of gradient array
    height : int
        The height of gradient array
    is_horizontal : bool
        Determines if horizontal or vertical gradation
    Returns
    ----------
    ndarray
    """

    if is_horizontal:
        return np.tile(np.linspace(start, stop, width), (height, 1))
    else:
        return np.tile(np.linspace(start, stop, height), (width, 1)).T
# get_gradation_2d


def get_gradation_3d(width, height, start_list, stop_list, is_horizontal_list):
    """Creates 3d gradient using get_gradation_2d.

    Expands to three dimensions. Sets start, stop, and is_horizontal for
    each color in a list, and uses the function for 2D, to create a
    gradation image for each channel.
    Parameters
    ----------
    start : int
        The starting value for gradation
    stop : int
        The stopping value for gradation
    start_list : 3-tuple(int)
        The width of gradient array
    stop_list : 3-tuple(int)
        The height of gradient array
    is_horizontal : 3-tuple(bool)
        Determines if horizontal or vertical gradation
    Returns
    ----------
    ndarray
    """
    result = np.zeros((height, width, len(start_list)), dtype=np.float)

    for i, (start, stop, is_horizontal) in enumerate(zip(start_list, stop_list, is_horizontal_list)):
        result[:, :, i] = get_gradation_2d(start, stop, width, height, is_horizontal)

    return result
# get_gradation_3d


def create_horizontal_gradation_mask():
    """Returns a black to white horizontal gradient Image

    Defines a function to return an image to use as a mask that fades from
    black to white left to right.
    Returns
    ----------
    Image
    """

    # creates a mask array that will create a dark to light gradation horizontally
    mask_array = get_gradation_3d(512, 256, (0, 0, 0), (255, 255, 255), (True, True, True))
    # creates a mask image from the array
    gradation_mask = Image.fromarray(np.uint8(mask_array)).convert("L")
    # returns image
    return gradation_mask
# create_gradation_mask


def create_vertical_gradation_mask():
    """Returns a black to white vertical gradient Image

    Defines a function to return an image to use as a mask that fades from
    black to white top down.
    Returns
    ----------
    Image
    """

    # creates a mask array that will create a dark to light gradation horizontally
    mask_array = get_gradation_3d(512, 256, (0, 0, 0), (255, 255, 255), (False, False, False))
    # creates a mask image from the array
    gradation_mask = Image.fromarray(np.uint8(mask_array)).convert("L")
    # returns image
    return gradation_mask
# create_vertical_gradation_mask


def create_vertical_split_mask_left():
    """Returns a black and white split Image

    Defines a function to return an image to use as a mask where the left half is
    black and the right half is white
    Returns
    ----------
    Image
    """

    # initializes a mask array with zeros(black) and specifies the type of the elements as 8-bit unsigned integers
    mask_array = np.zeros((512, 256), dtype=np.uint8)
    # assigns 255(white) to the all elements in the right half
    # of the array
    mask_array[:, 256 // 2:] = 255
    # creates a half black half white mask image from the array
    split_mask = Image.fromarray(mask_array, mode="L")
    # returns Image
    return split_mask
# create_vertical_split_mask


def create_vertical_split_mask_right():
    """Returns a black and white split Image

    Defines a function to return an image to use as a mask where the left half is
    white and the right half is black
    Returns
    ----------
    Image
    """

    # initializes a mask array with zeros(black) and specifies the type of the elements as 8-bit unsigned integers
    mask_array = np.zeros((512, 256), dtype=np.uint8)
    # assigns 255(white) to the all elements in the right half
    # of the array
    mask_array[:, : 256 // 2 ] = 255
    # creates a half black half white mask image from the array
    split_mask = Image.fromarray(mask_array, mode="L")
    # returns Image
    return split_mask
# create_vertical_split_mask


def create_vertical_lines_mask():
    """Returns a black and white Image with white vertical lines

    Defines a function to return an image to use as a mask where the white
    effect is in vertical lines
    Returns
    ----------
    Image
    """

    # initializes a mask array with zeros(black) and specifies the type of the elements as 8-bit unsigned integers
    # Uses a smaller size to make bigger lines when resized.
    mask_array = np.zeros((128, 32), dtype=np.uint8)

    # Iterates through the array to change every fifth pixel wide white
    for i in range(32):
        if i%5 == 0:
            mask_array[:,i] = 255

    # creates a mask image from the array
    mask = Image.fromarray(mask_array, mode="L")
    # returns Image
    return mask
# create_vertical_lines_mask


def create_horizontal_lines_mask():
    """Returns a black and white Image with white horizontal lines

    Defines a function to return an image to use as a mask where the white
    effect is in horizontal lines
    Returns
    ----------
    Image
    """

    # initializes a mask array with zeros(black) and specifies the type of the elements as 8-bit unsigned integers
    # Uses a smaller size to make bigger lines when resized.
    mask_array = np.zeros((32, 128), dtype=np.uint8)

    # Iterates through the array to change every fifth pixel wide white
    for i in range(32):
        if i%5 == 0:
            mask_array[i,:] = 255

    # creates a mask image from the array
    mask = Image.fromarray(mask_array, mode="L")
    # returns Image
    return mask
# create_vertical_lines_mask


def create_checkered_mask():
    """Returns a black and white Image with a checkerboard pattern of squares

    Defines a function to return an image to use as a mask where the white
    effect is in squares evenly spaced throughout the image
    Returns
    ----------
    Image
    """

    # Create new black image of entire board
    w, h = 12, 6
    mask = Image.new("L", (w,h))
    # loads pixel data into variable instead of opening an image
    pixels = mask.load()

    # Make pixels white where (row+col) is odd
    for i in range(w):
        for j in range(h):
            if (i+j)%2:
                pixels[i,j] = 255

    # returns Image
    return mask
# create_checkerd_mask

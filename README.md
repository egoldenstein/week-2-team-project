# Week 2 Team Project #
---

### Summary ###
modify-image.py is a program that will modify an image to have a unique effect applied to an image provided and can be blended with a mask.

**Contributors:** Emma Goldenstein, Elizabeth Gardner, Ella Nelson, Jakob Orel

An example: "python3 modify-image.py Images/leaves.JPG --ratio=4:3 --mask=horizontal-gradient --effect=4"
Before:
![Original Image](Images/leaves.JPG)
After:
![After modify-image](Images/leaves-modified.png)

### Installation and Use ###
To modify images of your own run the following terminal commands.
Note: You must have numpy and PIL installed. ("pip install ...")

```ssh
git clone https://jorel22@bitbucket.org/jorel22/week-2-team-project.git
cd week-2-team-project
python3 modify-image.py <image>
```

Description of optional arguments:

* \-\-ratio=int:int :enter a pair of integers separated by a colon to describe the desired aspect ratio (width to height ratio)

* \-\-mask=str      :enter a string to select the mask to blend the effect (horizontal-gradient, vertical-gradient, left, right, vertical-lines, horizontal-lines, checkered)

* \-\-effect=int    :enter an integer to select an effect to modify the image (0-6)

### How It Works ###
This program uses the Pillow (PIL fork), the Python Imaging Library, to manipulate images and apply different effects. It also uses the argparse module to parse command line arguments and the Numpy library for array mathematics.  

### Effects and Masks Available ###

#### Effects ####

* 0: Contours and enhances the edges

* 1: Increases contrast

* 2: Increases color

* 3: Posterizes image (removes number of available colors)

* 4: Solarizes image (inverts colors above threshold of 128)

* 5: Blurs image

* 6: Converts image to greyscale

#### Masks ####

* horizontal-gradient

* vertical-gradient

* left

* right

* vertical-lines

* horizontal-lines

* checkered

### TO DO ###
* Modify the image in different ways or places. Add different effects and masks.

### License ###

This project uses the [MIT license](https://bitbucket.org/jorel22/week-2-team-project/src/master/LICENSE.md).

"""
modify-image.py

Authors: Jakob Orel, Elizabeth Gardner, Ella Nelson, Emma Goldenstein
Date: 10-21-20

-reads a user-specified image file
-changes the aspect ratio of the image to a user-specified ratio
-applies different effects to different parts of the image
-saves the modified image in a file

"""
from PIL import Image, ImageFilter, ImageEnhance, ImageOps
import argparse
import createMask as cm


def main(args):
    #print("Testing Team Project!")

    # read an image from a file
    picture = Image.open(args.file_name)

    # produces a smaller version of the picture
    original_picture = picture.reduce(2)

    # shows original image for comparison
    original_picture.show()

    # creates the variables width and height with values
    # that correspond to the size of the given picture
    width, height = original_picture.size
    #print(original_picture.size)

    # crops the image to produce a new image in the desired aspect ratio
    ratios = args.ratio.split(":")
    w_ratio = int(ratios[0])
    h_ratio = int(ratios[1])
    # if the picture is too wide, cut off an equal amount from the left and right sides
    if width > ((w_ratio / h_ratio) * height):
        diff = (width - ((w_ratio / h_ratio) * height)) // 2
        left = diff
        upper = 0
        right = width - diff
        lower = height
        original_picture = original_picture.crop((left, upper, right, lower))
    # if the picture is too tall, cut off an equal amount from the top and bottom
    elif height > ((h_ratio / w_ratio) * width):
        diff = (height - ((h_ratio / w_ratio) * width)) // 2
        left = 0
        upper = diff
        right = width
        lower = height - diff
        original_picture = original_picture.crop((left, upper, right, lower))

    #print(original_picture.size)

    # Choose what type of effect to apply to image
    if args.effect == 0:
        # contours the picture
        altered_picture = original_picture.filter(ImageFilter.CONTOUR)
        # enhances the edges to make the image crisper
        altered_picture = altered_picture.filter(ImageFilter.EDGE_ENHANCE)
    elif args.effect == 1:
        # increases contrast
        enhancer = ImageEnhance.Contrast(original_picture)
        altered_picture = enhancer.enhance(3)
    elif args.effect == 2:
        # increases color
        enhancer = ImageEnhance.Color(original_picture)
        altered_picture = enhancer.enhance(3)
    elif args.effect == 3:
        # Removes number of of available colors to "posterize" image
        altered_picture = ImageOps.posterize(original_picture, 2)
    elif args.effect == 4:
        # Inverts colors above a certain threshold
        altered_picture = ImageOps.solarize(original_picture, threshold=128)
    elif args.effect == 5:
        # Blurs an image
        altered_picture = original_picture.filter(ImageFilter.GaussianBlur(4))
    elif args.effect == 6:
        # Converts an image to greyscale
        altered_picture = original_picture.convert("L")

    # shows altered picture for comparison
    altered_picture.show()

    # Choose what type of mask to use in Image.composite()
    if args.mask == "none":
        mask_image = Image.new("L", original_picture.size, color="white")
    elif args.mask == "horizontal-gradient":
        mask_image = cm.create_horizontal_gradation_mask()
        mask_image = mask_image.resize(original_picture.size)
    elif args.mask == "vertical-gradient":
        mask_image = cm.create_vertical_gradation_mask()
        mask_image = mask_image.resize(original_picture.size)
    elif args.mask == "left":
        mask_image = cm.create_vertical_split_mask_left()
        mask_image = mask_image.resize(original_picture.size)
    elif args.mask == "right":
        mask_image = cm.create_vertical_split_mask_right()
        mask_image = mask_image.resize(original_picture.size)
    elif args.mask == "vertical-lines":
        mask_image = cm.create_vertical_lines_mask()
        mask_image = mask_image.resize(original_picture.size)
    elif args.mask == "horizontal-lines":
        mask_image = cm.create_horizontal_lines_mask()
        mask_image = mask_image.resize(original_picture.size)
    elif args.mask == "checkered":
        mask_image = cm.create_checkered_mask()
        mask_image = mask_image.resize(original_picture.size)

    composite = Image.composite(altered_picture, original_picture, mask_image)
    # save new image with the original filename(-".ext") +"-modified"
    composite.save(f"{args.file_name[:-4]}-modified.png")
    # shows the composite image
    composite.show()


if __name__ == "__main__":
    # creates an ArgumentParser object
    parser = argparse.ArgumentParser()
    # adds a string argument for the purpose of indicating
    # what image the client wants to alter
    parser.add_argument("file_name", type=str, help="image file")
    # Optional argument to change aspect ratio must be int pair separated
    # by colon <int:int>
    parser.add_argument("--ratio", type=str, help="image's new aspect ratio", default="1:1")
    # Optional argument to select mask
    parser.add_argument("--mask", type=str, help="none, horizontal-gradient, vertical-gradient, left, right, vertical-lines, horizontal-lines, checkered",
                        default="none")
    # Optional argument to select image effect
    parser.add_argument("--effect", type=int, help="image effect (0-6)", default=0)
    # parses the arguments from the command line
    args = parser.parse_args()

    main(args)
